:title: A sample blog post
:date: 2019-02-25
:summary: A sample blog post for a website created using Pelican
:slug: sample-post
:category: general

This is a sample blog post for a website created
using Pelican.

**Some points to note:**

* The source file for this blog post is in 
  ``content/sample-post.rst``

* The value of ``:slug:`` in metadata should be
  unique and will be used to generate the URL
  for this post i.e., 
  http://localhost:8000/sample-post.html
