:title: About
:date: 2019-02-24

This is a sample page for a website created
with Pelican_.

*Some points to note:*

* The source file for this page is ``content/pages/about.rst``

* The syntax used here is reStructuredText

.. links

.. _Pelican: https://blog.getpelican.com

